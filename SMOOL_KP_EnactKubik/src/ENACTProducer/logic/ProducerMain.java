package ENACTProducer.logic;

import java.io.IOException;
import java.util.Observer;
import java.net.ServerSocket;
import java.net.Socket;

import org.smool.kpi.model.exception.KPIModelException;

import ENACTProducer.api.BlindPositionActuatorSubscription;
import ENACTProducer.api.Consumer;
import ENACTProducer.api.Producer;
import ENACTProducer.api.SmoolKP;
import ENACTProducer.model.smoolcore.IContinuousInformation;
import ENACTProducer.model.smoolcore.impl.BlindPositionActuator;

import ENACTProducer.model.smoolcore.impl.TemperatureInformation;
import ENACTProducer.model.smoolcore.impl.TemperatureSensor;
import ENACTProducer.model.smoolcore.impl.HumiditySensor;
import ENACTProducer.model.smoolcore.impl.HumidityInformation;
import ENACTProducer.model.smoolcore.impl.PresenceSensor;
import ENACTProducer.model.smoolcore.impl.PresenceInformation;
import ENACTProducer.model.smoolcore.impl.LightingSensor;
import ENACTProducer.model.smoolcore.impl.Message;
import ENACTProducer.model.smoolcore.impl.MessageReceiveSensor;
import ENACTProducer.model.smoolcore.impl.LightingInformation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

//import org.smool.kpi.common.Logger;

/**
 * Producer of data for the Smart Building use case for Enact.
 * <p>
 * NOTE: although this app is mainly a producer of sensors data, it can receive
 * actuation orders.
 * </p>
 * <p>
 * This app is sending (via SMOOL) the temperature, gas, etc stataus in the
 * smart building. When an external app controlling this data decides that the
 * values must be re-adapted, an actuation order is sent back to this
 * appication, and this one check if security data is valid and then it calls
 * the Smart Building SCADA to tune the values.
 * </p>
 *
 */
public class ProducerMain {

	public static final String vendor = "Tecnalia";
	public static final String name = "EnactKubik" + System.currentTimeMillis() % 10000;
	private static ServerSocket ss;
	private static Socket cs;
	private static ServerSocket ssObserver;
	private static Socket csObserver;

	public ProducerMain(String sib, String addr, int port, int inTcpPort, int outTcpPort) throws Exception {
		SmoolKP.setKPName(name);
		System.out.println("*** " + name + " ***");
		// ---------------------------CONNECT TO SMOOL---------------------
		// SmoolKP.connect();
		// SmoolKP.connect("sib1", "172.24.5.151", 23000);

		SmoolKP.connect(sib, addr, port);

		// ---------------------------CREATE SENSOR-----------------------------
		Producer producer = SmoolKP.getProducer();

		PresenceSensor pirSensorP0M1 = new PresenceSensor(name + "pirSensorP0M1");
		TemperatureSensor tempSensorP0M1 = new TemperatureSensor(name + "tempSensorP0M1");
		HumiditySensor humiditySensorP0M1 = new HumiditySensor(name + "humiditySensorP0M1");
		LightingSensor lightSensorP0M1 = new LightingSensor(name + "lightSensorP0M1");

		PresenceSensor pirSensorP0S1 = new PresenceSensor(name + "pirSensorP0S1");
		TemperatureSensor tempSensorP0S1 = new TemperatureSensor(name + "tempSensorP0S1");
		HumiditySensor humiditySensorP0S1 = new HumiditySensor(name + "humiditySensorP0S1");
		LightingSensor lightSensorP0S1 = new LightingSensor(name + "lightSensorP0S1");

		PresenceSensor pirSensorP0S2 = new PresenceSensor(name + "pirSensorP0S2");
		TemperatureSensor tempSensorP0S2 = new TemperatureSensor(name + "tempSensorP0S2");
		HumiditySensor humiditySensorP0S2 = new HumiditySensor(name + "humiditySensorP0S2");
		LightingSensor lightSensorP0S2 = new LightingSensor(name + "lightSensorP0S2");

		PresenceSensor pirSensorP0S3 = new PresenceSensor(name + "pirSensorP0S3");
		TemperatureSensor tempSensorP0S3 = new TemperatureSensor(name + "tempSensorP0S3");
		HumiditySensor humiditySensorP0S3 = new HumiditySensor(name + "humiditySensorP0S3");
		LightingSensor lightSensorP0S3 = new LightingSensor(name + "lightSensorP0S3");
		
		TemperatureSensor tempSensorOutdoor = new TemperatureSensor(name + "tempSensorOutdoor");
        MessageReceiveSensor radSensorEastSolar= new MessageReceiveSensor(name + "radSensorEastSolar");
        MessageReceiveSensor radSensorNorthSolar= new MessageReceiveSensor(name + "radSensorNorthSolar");
        
        
		// ---------------------------PRODUCE DATA----------------------------------
		String timestamp = Long.toString(System.currentTimeMillis());

		PresenceInformation pirInfoP0M1 = new PresenceInformation(name + "_pirP0M1");
		boolean pirP0M1 = false;
		pirInfoP0M1.setStatus(pirP0M1).setTimestamp(timestamp);
		producer.createPresenceSensor(pirSensorP0M1._getIndividualID(), name, vendor, null, null, pirInfoP0M1);

		TemperatureInformation tempInfoP0M1 = new TemperatureInformation(name + "_tempP0M1");
		double tempP0M1 = 25;
		tempInfoP0M1.setValue(tempP0M1).setUnit("�C").setTimestamp(timestamp);

		producer.createTemperatureSensor(tempSensorP0M1._getIndividualID(), name, vendor, null, null, tempInfoP0M1);

		HumidityInformation humidityInfoP0M1 = new HumidityInformation(name + "_humidityP0M1");
		double humidityP0M1 = 50;
		humidityInfoP0M1.setValue(humidityP0M1).setUnit("%").setTimestamp(timestamp);
		producer.createHumiditySensor(humiditySensorP0M1._getIndividualID(), name, vendor, null, humidityInfoP0M1,
				null);

		LightingInformation lightInfoP0M1 = new LightingInformation(name + "_lightP0M1");
		double lightP0M1 = 100;
		lightInfoP0M1.setValue(lightP0M1).setUnit("Lux").setTimestamp(timestamp);
		producer.createLightingSensor(lightSensorP0M1._getIndividualID(), name, vendor, null, lightInfoP0M1, null);

		PresenceInformation pirInfoP0S1 = new PresenceInformation(name + "_pirP0S1");
		boolean pirP0S1 = false;
		pirInfoP0S1.setStatus(pirP0S1).setTimestamp(timestamp);
		producer.createPresenceSensor(pirSensorP0S1._getIndividualID(), name, vendor, null, null, pirInfoP0S1);

		TemperatureInformation tempInfoP0S1 = new TemperatureInformation(name + "_tempP0S1");
		double tempP0S1 = 25;
		tempInfoP0S1.setValue(tempP0S1).setUnit("�C").setTimestamp(timestamp);
		producer.createTemperatureSensor(tempSensorP0S1._getIndividualID(), name, vendor, null, null, tempInfoP0S1);

		HumidityInformation humidityInfoP0S1 = new HumidityInformation(name + "_humidityP0S1");
		double humidityP0S1 = 50;
		humidityInfoP0S1.setValue(humidityP0S1).setUnit("%").setTimestamp(timestamp);
		producer.createHumiditySensor(humiditySensorP0S1._getIndividualID(), name, vendor, null, humidityInfoP0S1,
				null);

		LightingInformation lightInfoP0S1 = new LightingInformation(name + "_lightP0S1");
		double lightP0S1 = 100;
		lightInfoP0S1.setValue(lightP0S1).setUnit("Lux").setTimestamp(timestamp);
		producer.createLightingSensor(lightSensorP0S1._getIndividualID(), name, vendor, null, lightInfoP0S1, null);

		PresenceInformation pirInfoP0S2 = new PresenceInformation(name + "_pirP0S2");
		boolean pirP0S2 = false;
		pirInfoP0S2.setStatus(pirP0S2).setTimestamp(timestamp);
		producer.createPresenceSensor(pirSensorP0S2._getIndividualID(), name, vendor, null, null, pirInfoP0S2);

		TemperatureInformation tempInfoP0S2 = new TemperatureInformation(name + "_tempP0S2");
		double tempP0S2 = 25;
		tempInfoP0S2.setValue(tempP0S2).setUnit("�C").setTimestamp(timestamp);
		producer.createTemperatureSensor(tempSensorP0S2._getIndividualID(), name, vendor, null, null, tempInfoP0S2);

		HumidityInformation humidityInfoP0S2 = new HumidityInformation(name + "_humidityP0S2");
		double humidityP0S2 = 50;
		humidityInfoP0S2.setValue(humidityP0S2).setUnit("%").setTimestamp(timestamp);
		producer.createHumiditySensor(humiditySensorP0S2._getIndividualID(), name, vendor, null, humidityInfoP0S2,
				null);

		LightingInformation lightInfoP0S2 = new LightingInformation(name + "_lightP0S2");
		double lightP0S2 = 100;
		lightInfoP0S2.setValue(lightP0S2).setUnit("Lux").setTimestamp(timestamp);
		producer.createLightingSensor(lightSensorP0S2._getIndividualID(), name, vendor, null, lightInfoP0S2, null);

		PresenceInformation pirInfoP0S3 = new PresenceInformation(name + "_pirP0S3");
		boolean pirP0S3 = false;
		pirInfoP0S3.setStatus(pirP0S3).setTimestamp(timestamp);
		producer.createPresenceSensor(pirSensorP0S3._getIndividualID(), name, vendor, null, null, pirInfoP0S3);

		TemperatureInformation tempInfoP0S3 = new TemperatureInformation(name + "_tempP0S3");
		double tempP0S3 = 25;
		tempInfoP0S3.setValue(tempP0S3).setUnit("�C").setTimestamp(timestamp);
		producer.createTemperatureSensor(tempSensorP0S3._getIndividualID(), name, vendor, null, null, tempInfoP0S3);

		HumidityInformation humidityInfoP0S3 = new HumidityInformation(name + "_humidityP0S3");
		double humidityP0S3 = 50;
		humidityInfoP0S3.setValue(humidityP0S3).setUnit("%").setTimestamp(timestamp);
		producer.createHumiditySensor(humiditySensorP0S3._getIndividualID(), name, vendor, null, humidityInfoP0S3,
				null);

		LightingInformation lightInfoP0S3 = new LightingInformation(name + "_lightP0S3");
		double lightP0S3 = 100;
		lightInfoP0S3.setValue(lightP0S3).setUnit("Lux").setTimestamp(timestamp);
		producer.createLightingSensor(lightSensorP0S3._getIndividualID(), name, vendor, null, lightInfoP0S3, null);


		TemperatureInformation tempInfoOutdoor = new TemperatureInformation(name + "_tempOutdoor");
		double tempOutdoor = 25;
		tempInfoOutdoor.setValue(tempOutdoor).setUnit("�C").setTimestamp(timestamp);
		producer.createTemperatureSensor(tempSensorOutdoor._getIndividualID(), name, vendor, null, null, tempInfoOutdoor);
		
		Message radInfoEastSolar = new Message(name + "_radEastSolar");
		double radEastSolar = 80;
		radInfoEastSolar.setBody(Double.toString(radEastSolar)).setTimestamp(timestamp);
		producer.createMessageReceiveSensor(radSensorEastSolar._getIndividualID(), name, vendor, null, null, null, radInfoEastSolar, null);
		
		Message radInfoNorthSolar = new Message(name + "_radNorthSolar");
		double radNorthSolar = 80;
		radInfoNorthSolar.setBody(Double.toString(radNorthSolar));
		producer.createMessageReceiveSensor(radSensorNorthSolar._getIndividualID(), name, vendor, null, null, null, radInfoNorthSolar, null);
		
		// --------------------------STABLISH TCP CONNECTION--------------------------
		ss = new ServerSocket(inTcpPort);
		cs = new Socket();
		
	    ssObserver = new ServerSocket(outTcpPort);
		csObserver = new Socket();
		
		// ---------------------------CONSUME ACTION----------------------------------
		Consumer consumer = SmoolKP.getConsumer();
		BlindPositionActuatorSubscription subscription = new BlindPositionActuatorSubscription(createObserver());
		consumer.subscribeToBlindPositionActuator(subscription, null);

		String msg;
		BufferedReader b = null;
		
		while (true) {
			if(!cs.isConnected())
			{
				cs = ss.accept();
			}
			System.out.println("Client connected");
			b = new BufferedReader(new InputStreamReader(cs.getInputStream()));
			while ((msg = b.readLine()) != null) {

				// ---------------------SEND DATA--------------------------------------------
				timestamp = Long.toString(System.currentTimeMillis());
				System.out.println("Producing data at " + timestamp);
				String[] keyValue = msg.split(":");
				if (keyValue.length > 1) 
				{
					String key = keyValue[0];
					Double value = Double.parseDouble(keyValue[1]);

					switch (keyValue[0]) {
					case "pirP0M1": {
						pirP0M1 = (keyValue[1]).equals("1") ? true : false;
						pirInfoP0M1.setStatus(pirP0M1).setTimestamp(timestamp);
						producer.updatePresenceSensor(pirSensorP0M1._getIndividualID(), name, vendor, null, null,
								pirInfoP0M1);
						System.out.println("Producing pirP0M1  " + pirP0M1);
						break;
					}
					case "tempP0M1": {
						tempP0M1 = Double.parseDouble(keyValue[1]);
						tempInfoP0M1.setValue(tempP0M1).setTimestamp(timestamp);
						producer.updateTemperatureSensor(tempSensorP0M1._getIndividualID(), name, vendor, null, null,
								tempInfoP0M1);

						System.out.println("Producing tempP0M1  " + tempP0M1);
						break;
					}
					case "humidityP0M1": {
						humidityP0M1 = Double.parseDouble(keyValue[1]);
						humidityInfoP0M1.setValue(humidityP0M1).setTimestamp(timestamp);
						producer.updateHumiditySensor(humiditySensorP0M1._getIndividualID(), name, vendor, null,
								humidityInfoP0M1, null);
						System.out.println("Producing humidityP0M1  " + humidityP0M1);
						break;
					}
					case "lightP0M1": {
						lightP0M1 = Double.parseDouble(keyValue[1]);
						lightInfoP0M1.setValue(lightP0M1).setTimestamp(timestamp);
						producer.updateLightingSensor(lightSensorP0M1._getIndividualID(), name, vendor, null,
								lightInfoP0M1, null);
						System.out.println("Producing lightP0M1  " + lightP0M1);
						break;
					}
					case "pirP0S1": {
						pirP0S1 = (keyValue[1]).equals("1") ? true : false;
						pirInfoP0S1.setStatus(pirP0S1).setTimestamp(timestamp);
						producer.updatePresenceSensor(pirSensorP0S1._getIndividualID(), name, vendor, null, null,
								pirInfoP0S1);
						System.out.println("Producing pirP0S1  " + pirP0S1);
						break;
					}
					case "tempP0S1": {
						tempP0S1 = Double.parseDouble(keyValue[1]);
						tempInfoP0S1.setValue(tempP0S1).setTimestamp(timestamp);
						producer.updateTemperatureSensor(tempSensorP0S1._getIndividualID(), name, vendor, null, null,
								tempInfoP0S1);
						System.out.println("Producing tempP0S1  " + tempP0S1);
						break;
					}
					case "humidityP0S1": {
						humidityP0S1 = Double.parseDouble(keyValue[1]);
						humidityInfoP0S1.setValue(humidityP0S1).setTimestamp(timestamp);
						producer.updateHumiditySensor(humiditySensorP0S1._getIndividualID(), name, vendor, null,
								humidityInfoP0S1, null);
						System.out.println("Producing humidityP0S1  " + humidityP0S1);
						break;
					}
					case "lightP0S1": {
						lightP0S1 = Double.parseDouble(keyValue[1]);
						lightInfoP0S1.setValue(lightP0S1).setTimestamp(timestamp);
						producer.updateLightingSensor(lightSensorP0S1._getIndividualID(), name, vendor, null,
								lightInfoP0S1, null);
						System.out.println("Producing lightP0S1  " + lightP0S1);
						break;
					}
					case "pirP0S2": {
						pirP0S2 = (keyValue[1]).equals("1") ? true : false;
						pirInfoP0S2.setStatus(pirP0S2).setTimestamp(timestamp);
						producer.updatePresenceSensor(pirSensorP0S2._getIndividualID(), name, vendor, null, null,
								pirInfoP0S2);
						System.out.println("Producing pirP0S2  " + pirP0S2);
						break;
					}
					case "tempP0S2": {
						tempP0S2 = Double.parseDouble(keyValue[1]);
						tempInfoP0S2.setValue(tempP0S2).setTimestamp(timestamp);
						producer.updateTemperatureSensor(tempSensorP0S2._getIndividualID(), name, vendor, null, null,
								tempInfoP0S2);
						System.out.println("Producing tempP0S2  " + tempP0S2);
						break;
					}
					case "humidityP0S2": {
						humidityP0S2 = Double.parseDouble(keyValue[1]);
						humidityInfoP0S2.setValue(humidityP0S2).setTimestamp(timestamp);
						producer.updateHumiditySensor(humiditySensorP0S2._getIndividualID(), name, vendor, null,
								humidityInfoP0S2, null);
						System.out.println("Producing humidityP0S2  " + humidityP0S2);
						break;
					}
					case "lightP0S2": {
						lightP0S2 = Double.parseDouble(keyValue[1]);
						lightInfoP0S2.setValue(lightP0S2).setTimestamp(timestamp);
						producer.updateLightingSensor(lightSensorP0S2._getIndividualID(), name, vendor, null,
								lightInfoP0S2, null);
						System.out.println("Producing lightP0S2  " + lightP0S2);
						break;
					}
					case "pirP0S3": {
						pirP0S3 = (keyValue[1]).equals("1") ? true : false;
						pirInfoP0S3.setStatus(pirP0S3).setTimestamp(timestamp);
						producer.updatePresenceSensor(pirSensorP0S3._getIndividualID(), name, vendor, null, null,
								pirInfoP0S3);
						System.out.println("Producing pirP0S3  " + pirP0S3);
						break;
					}
					case "tempP0S3": {
						tempP0S3 = Double.parseDouble(keyValue[1]);
						tempInfoP0S3.setValue(tempP0S3).setTimestamp(timestamp);
						producer.updateTemperatureSensor(tempSensorP0S3._getIndividualID(), name, vendor, null, null,
								tempInfoP0S3);
						System.out.println("Producing tempP0S3  " + tempP0S3);
						break;
					}
					case "humidityP0S3": {
						humidityP0S3 = Double.parseDouble(keyValue[1]);
						humidityInfoP0S3.setValue(humidityP0S3).setTimestamp(timestamp);
						producer.updateHumiditySensor(humiditySensorP0S3._getIndividualID(), name, vendor, null,
								humidityInfoP0S3, null);
						System.out.println("Producing humidityP0S3  " + humidityP0S3);
						break;
					}
					case "lightP0S3": {
						lightP0S3 = Double.parseDouble(keyValue[1]);
						lightInfoP0S3.setValue(lightP0S3).setTimestamp(timestamp);
						producer.updateLightingSensor(lightSensorP0S3._getIndividualID(), name, vendor, null,
								lightInfoP0S3, null);
						System.out.println("Producing lightP0S3  " + lightP0S3);
						break;
					}
					case "tempOutdoor": {
						tempOutdoor = Double.parseDouble(keyValue[1]);
						tempInfoOutdoor.setValue(tempOutdoor).setTimestamp(timestamp);
						producer.updateTemperatureSensor(tempSensorOutdoor._getIndividualID(), name, vendor, null, null,
								tempInfoOutdoor);
						System.out.println("Producing tempOutdoor  " + tempOutdoor);
						break;
					}
					case "radEastSolar": {
						radEastSolar = Double.parseDouble(keyValue[1]);
						radInfoEastSolar.setBody(Double.toString(radEastSolar)).setTimestamp(timestamp);
						producer.updateMessageReceiveSensor(radSensorEastSolar._getIndividualID(), name, vendor, null, null,null,
								radInfoEastSolar,null);
						System.out.println("Producing radEastSolar  " + radEastSolar);
						break;
					}
					case "radNorthSolar": {
						radNorthSolar = Double.parseDouble(keyValue[1]);
						radInfoEastSolar.setBody(Double.toString(radNorthSolar)).setTimestamp(timestamp);
						producer.updateMessageReceiveSensor(radSensorNorthSolar._getIndividualID(), name, vendor, null, null,null,
								radInfoNorthSolar,null);
						System.out.println("Producing radEastSolar  " + radNorthSolar);
						break;
					}
					}
				}
				
			}
			b.close();
			cs.close();
			System.out.println("EXIT");
		}

	}

	/**
	 * Processs messages related to actuation orders on blinds
	 */
	private Observer createObserver() {
		return (o, concept) -> {
			BlindPositionActuator actuator = (BlindPositionActuator) concept;
			IContinuousInformation info = actuator.getBlindPos();

			System.out.println("receiving ACTUATION order on blinds: " + info.getDataID() + " and security "
					+ info.getSecurityData().getData());

			try {
				if (info.getDataID() != null) {
					if(!csObserver.isConnected())
				    {
						csObserver=ssObserver.accept();
					}
					
					PrintWriter blindActuation = new PrintWriter(
							new BufferedWriter(new OutputStreamWriter(csObserver.getOutputStream())), true);
					blindActuation.println(info.getDataID());
					System.out.println(info.getDataID() + " blind actuation sent");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
	}

	boolean test(String data) {
		return true;
	}

	private Observer myObserver() {
		return (o, concept) -> {
			BlindPositionActuator actuator = (BlindPositionActuator) concept;
			IContinuousInformation info = actuator.getBlindPos();
			System.out.println(info.getValue());
		};
	}

	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		int inTcpPort = args.length > 3 ? Integer.valueOf(args[3]) : 2005;
		int outTcpPort = args.length > 4 ? Integer.valueOf(args[4]) : 2007;
		// Logger.setDebugging(true);
		// Logger.setDebugLevel(4);
		while (true) {
			try {
				new ProducerMain(sib, addr, port,inTcpPort,outTcpPort);
			} catch (KPIModelException | IOException e) {
				if (cs != null) {
					cs.close();
					ss.close();
				}
				if (csObserver != null) {
					csObserver.close();
					ssObserver.close();
				}
				e.printStackTrace();
				Thread.sleep(1000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				if (cs != null) {
					cs.close();
					ss.close();
				}
				if (csObserver != null) {
					csObserver.close();
					ssObserver.close();
				}
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
