package ENACTConsumer.logic;

import java.io.IOException;
import org.smool.kpi.model.exception.KPIModelException;
import ENACTConsumer.api.Consumer;
import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.api.TemperatureSensorSubscription;
import ENACTConsumer.model.smoolcore.impl.TemperatureSensor;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;






/**
 * Subscribe to data generated from KUBIK smart building.
 * <p>
 * This class should be the skeleton for the IoT application in the SB use case
 * </p>
 * <p>
 * The app is retrieving temperature and other data. When temp is higher or
 * lower than comfort, an actuation order is sent back to the Smart Building to
 * turn the temperature back to normal.
 * </p>
 *
 */
public class ConsumerMain {
	public static final String name = "EnactKubikConsumer" + System.currentTimeMillis() % 10000;
	public static CustomActuation blindLivingUpActuation;
	public static CustomActuation blindLivingDownActuation;
	public static CustomActuation blindKitchenUpActuation;
	public static CustomActuation blindKitchenDownActuation;
	private static ServerSocket ss;
	private static Socket cs;

	public ConsumerMain(String sib, String addr, int port,int tcpPort ) throws Exception {

		SmoolKP.setKPName(name);
		System.out.println("*** " + name + " ***");
		
		// ------------PREPARE ACTUATION OBJECT--------------------
		blindLivingUpActuation =new CustomActuation(name,"blindLiving","Up");
		blindLivingDownActuation =new CustomActuation(name,"blindLiving","Down");
		blindKitchenUpActuation =new CustomActuation(name,"blindKitchen","Up");
		blindKitchenDownActuation =new CustomActuation(name,"blindKitchen","Down");

		// ------------CONNECT TO SMOOL----------------------------
		// SmoolKP.connect();
		SmoolKP.connect(sib, addr, port);

		// ------------SUBSCRIBE TO DATA---------------------------
		//Consumer consumer = SmoolKP.getConsumer();
		//consumer.subscribeToTemperatureSensor(new TemperatureSensorSubscription(createTemperatureObserver()), null);
		
		//-------------SEND ACTUATION-------------------------------
		String timestamp = Long.toString(System.currentTimeMillis());
		
		ss = new ServerSocket(tcpPort);
		cs = new Socket();
		 // maximum interval that at least one message should arrive

		
		String msg;
		BufferedReader b = null;
		while (true) {
			
			if(!cs.isConnected())
			{
				cs = ss.accept();
			}
			System.out.println("Client connected");
			b = new BufferedReader(new InputStreamReader(cs.getInputStream()));
			while ((msg = b.readLine()) != null) {
				SmoolKP.lastTimestamp=System.currentTimeMillis();
				// ---------------------SEND DATA--------------------------------------------
				timestamp = Long.toString(System.currentTimeMillis());
				System.out.println("Consuming data at " + timestamp);
				System.out.println("Msg: " + msg);
				switch (msg) {
				case "blindLivingUp": {
					new Thread(() -> ConsumerMain.blindLivingUpActuation.run(0)).start(); // ConsumerMain.actuation.run();
					System.out.println("Run blindLivingUpActuation");
					break;
				}
				case "blindLivingDown": {
					new Thread(() -> ConsumerMain.blindLivingDownActuation.run(0)).start(); // ConsumerMain.actuation.run();
					System.out.println("Run blindLivingDownActuation");
					break;

				}
				case "blindKitchenUp": {
					new Thread(() -> ConsumerMain.blindKitchenUpActuation.run(0)).start(); // ConsumerMain.actuation.run();
					System.out.println("Run blindKitchenUpActuation");
					break;
				}
				case "blindKitchenDown": {
					new Thread(() -> ConsumerMain.blindKitchenDownActuation.run(0)).start(); // ConsumerMain.actuation.run();
					System.out.println("Run blindKitchenDownActuation");
					break;

				}

				}
				Thread.sleep(1000);
				
				SmoolKP.checkConnection(3600);
				
				
			}
			b.close();
			cs.close();
			System.out.println("EXIT");
		}
		// ------------ATTACH WATCHDOG instead of SLEEP------------
		// Thread.sleep(Long.MAX_VALUE); // keep application alive.
		//SmoolKP.watchdog(3600); // maximum interval that at least one message should arrive
	}

	
	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		int tcpPort = args.length > 3 ? Integer.valueOf(args[3]) : 2006;
		while (true) {
			try {
				new ConsumerMain(sib, addr, port,tcpPort);
			} catch (KPIModelException | IOException e) {
				if (cs != null) {
					cs.close();
					ss.close();
				}
				e.printStackTrace();
				Thread.sleep(10000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				if (cs != null) {
					cs.close();
					ss.close();
				}
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
