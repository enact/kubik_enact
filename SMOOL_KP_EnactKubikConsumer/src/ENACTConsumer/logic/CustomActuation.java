package ENACTConsumer.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.model.smoolcore.impl.ContinuousInformation;
import ENACTConsumer.model.smoolcore.impl.SecurityAuthorization;
import ENACTConsumer.model.smoolcore.impl.BlindPositionActuator;

/**
 * Example of sending back to SMOOL am actuation message with security data.
 * 
 * <p>
 * Once a temperature value is reached, the temperature Observer calls this
 * class. Then, a new Actuation message on blinds is sent. To prevent other
 * clients to send actuation orders, this message is sent with security content.
 * </P>
 */
class CustomActuation {
	
	private static final String SECURITY_ENDPOINT = "https://15.236.132.74:8443/jwt";
	private boolean isFirstActuation = true;
	private String kpName;
	private String name;
	private String act;
	private double val = 0;
	private static int counter=0;
	private BlindPositionActuator actuator;


	SecurityAuthorization sec;
	private ContinuousInformation blindPos;
	
	public CustomActuation(String kpName,String name, String act) {
		this.kpName = kpName;
		//this.name = kpName + "_blindActuator";
		this.name=name;
		this.act=act;
		actuator = new BlindPositionActuator(name + "_actuator");
		blindPos = new ContinuousInformation(name+ "_blindPosition" );	
		sec = new SecurityAuthorization(kpName + "_blindActuator_security");
	}
	
	public synchronized void run(double temp) {
		try {
			System.out.println("Sending ACTUATION order for " + name);
			
			// create the security metadata
			String token = HTTPS.post(SECURITY_ENDPOINT, "{\"sub\":\""+kpName+"\",\"obj\": \""+ name +"\", \"act\": \" "+ act+"\"}");
			sec=new SecurityAuthorization(kpName + "_blindActuator_security"+Integer.toString(counter++));
			sec.setType("JWT + CASBIN payload").setData(token).setTimestamp(Long.toString(System.currentTimeMillis()));
			
			// create the blindPosition information
			blindPos.setValue(val++).setSecurityData(sec).setTimestamp(Long.toString(System.currentTimeMillis()));
			blindPos.setDataID(name+act);
		
			actuator.setBlindPos(blindPos);
			// send to SMOOL
			if (isFirstActuation) {
								
				SmoolKP.getProducer().createBlindPositionActuator(name+act, kpName, "TECNALIA", null, blindPos, null);
				isFirstActuation=false;
			}else {
				SmoolKP.getProducer().updateBlindPositionActuator(name+act, kpName, "TECNALIA", null, blindPos, null);
			}
		} catch (Exception e) {
			System.err.println("Error: the actuation order cannot be sent. " + e.getMessage());
		}
	}
	
	
}