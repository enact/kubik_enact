**SECURE ACTUATION IN KUBIK BUILDING USING SMOOL MIDLEWARE**

SMOOL KPs and Node-RED flows:
- **SMOOL_KP_EnactKubik**: EnactKubik KP, adaptation of the SMOOL producer to make secure actuation
- **SMOOL_KP_EnactKubikConsumer**: EnactKubikConsumer KP, adaptation of the SMOOL consumer to make secure actuation
- **Node-RED_code**: Code to adapt IoT messages from Z-Wave devices, fancoils and meteo data to the SMOOL middleware
